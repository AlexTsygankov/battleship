﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BtSh_Model
{
    public class Ship
    {
        public ShipTypes Type { get; set; }
        public bool Gorizontal { get; set; }
        public int Health { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        private Rectangle rect;                 // Прямоугольник размером с корабль
        public Rectangle Bounds { get { return new Rectangle(rect.Location, rect.Size); } }

        public bool Contains(int x, int y) { return this.rect.Contains(x, y); }

        public Ship() { }
        public Ship(ShipTypes type, bool gorizon)
        {
            this.Type = type;
            this.Gorizontal = gorizon;
            //this.Health = (int)this.Type;
        }
        public Ship(int x, int y, ShipTypes type, bool gorizon)
        {
            Construct(x, y, type, gorizon);
        }

        private void Construct(int x, int y, ShipTypes type, bool gorizon)
        {
            this.X = x;
            this.Y = y;
            this.Type = type;
            this.Gorizontal = gorizon;
            //this.Health = (int)type;

            if (this.Gorizontal)
                this.rect = new Rectangle(this.X, this.Y, (int)this.Type, 1);
            else
                this.rect = new Rectangle(this.X, this.Y, 1, (int)this.Type);
        }

        public override string ToString()
        {
            return string.Format("{0}*{1}*{2}*{3}", Health, X, Y, this.Gorizontal);
        }

        public void FromString(string ship)
        {
            string[] parts = ship.Split('*');
            int x, y, heal;
            bool goriz;

            if (!int.TryParse(parts[0], out heal))
                throw new ArgumentException("нераспарсил Health " + parts[0] + "   '" + ship + "'");
            if (!int.TryParse(parts[1], out x))
                throw new ArgumentException("нераспарсил X " + parts[1] + "   '" + ship + "'");
            if (!int.TryParse(parts[2], out y))
                throw new ArgumentException("нераспарсил Y " + parts[2] + "   '" + ship + "'");
            if (!bool.TryParse(parts[3], out goriz))
                throw new ArgumentException("нераспарсил Gorizontal " + parts[3] + "   '" + ship + "'");

            Construct(x, y, (ShipTypes)heal, goriz);
        }

        //public ShipTypes ShipTypes
        //{
        //    get
        //    {
        //        throw new System.NotImplementedException();
        //    }
        //    set
        //    {
        //    }
        //}
    }
}
