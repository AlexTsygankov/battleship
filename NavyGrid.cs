﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//using System.Threading.Tasks;

namespace BattleShips_OOP
{
    /***************************************************************
     *  НЕ ТРОГАЙ public У этих классов и перечислений!!
     * 
     * Он меняет уровень доступа. Без него класс становится privat
     * и SetData() из второй формы теряет право доступа. 
     * Возникает ошибка при компиляции
     ***************************************************************/



    /// <summary>
    /// Состояние последнего выстрела
    /// </summary>
    public enum HitStatus 
    { /*Default = 0,*/ Miss = 0, Hit, Kill }



    /// <summary>
    ///  Перечисление состояний ячейки поля.
    ///  Mark = красное поле при построении. Буферная зона.
    /// </summary>
    public enum CellStatuses : byte
    { Empty = 0, Ship = 1, DestroyedPart = 2, Miss = 3, Mark = 4 }
    
    /// <summary>
    /// Типы кораблей
    /// </summary>
    public enum ShipTypes : byte
    { submarine = 1, destroyer, cruiser, battleship }
    
    public class NavyGrid 
    {
        public HitStatus lastHitStat { get; set; }
        // Последнее попадание
        public Point lastHitPoint { get; set; }
        public CellStatuses[,] Cells { get; set; }   // Игровое поле по ячейкам
        public Ship[] Ships { get; set; }               // Все доступные корабли во флотилии

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="CellGridWidth"> Кол-во ячеек по горизонтали</param>
        /// <param name="CellGridHeight"> Кол-во ячеек по вертикали</param>
        public NavyGrid(int CellGridWidth = 10, int CellGridHeight = 10)
        {
            this.Cells = new CellStatuses[CellGridHeight, CellGridWidth];

            for (int j = 0; j < CellGridHeight; j++)
                for (int i = 0; i < CellGridWidth; i++)
                    this.Cells[j, i] = CellStatuses.Empty;
            this.Ships = new Ship[10];

            this.lastHitPoint = new Point(-1, -1);
            this.lastHitStat = HitStatus.Miss;
        }
        public void setLastHitPoint(int x, int y)
        {
            this.lastHitPoint = new Point(x, y);
        }
    }
}
