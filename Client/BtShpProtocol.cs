﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleShips_OOP
{
    public abstract class BtShpProtocol
    {
        public static readonly Dictionary<MessagesProtocol, string> ProtocolDictionary = new Dictionary<MessagesProtocol, string>{
            {MessagesProtocol.start_game, "start_game"},
            {MessagesProtocol.end_game, "end_game"},
            {MessagesProtocol.hit, "hit"},
            {MessagesProtocol.kill, "kill"},
            {MessagesProtocol.miss, "miss"},
            {MessagesProtocol.ask, "ask"},
            {MessagesProtocol.answer, "answer"},
            {MessagesProtocol.ships, "ships"},
            {MessagesProtocol.shoot, "shoot"}
        };
        public enum MessagesProtocol : byte { start_game, end_game, shoot, miss, hit, kill, ask, answer, ships}
    }
}
