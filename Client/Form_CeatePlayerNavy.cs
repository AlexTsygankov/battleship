﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace BattleShips_OOP
{
    public partial class Form_CeatePlayerNavy : Form
    {
        private Ship tempShip = new Ship(ShipTypes.battleship, true);
        private NavyGrid PlayerNavy = new NavyGrid();
        private int PlayerShipsCounter = 0;

        private const int constCellWidth = 30;
        private const int CellCountW = 10;
        private const int CellCountH = 10;       

        private int[] CountFreeShips = new int[4];

        public Form_CeatePlayerNavy()
        {
            InitializeComponent();
            Bitmap bitPanel1BackgroungImage = new Bitmap(panel1.Width, panel1.Height);
            panel1.BackgroundImage = bitPanel1BackgroungImage;
                        
            CountFreeShips[0] = 4; 
            CountFreeShips[1] = 3; 
            CountFreeShips[2] = 2;
            CountFreeShips[3] = 1;          
        }

        private void Form_CeatePlayerNavy_Load(object sender, EventArgs e)
        {
            ServiceInstrument.DrawGrid(Graphics.FromImage(panel1.BackgroundImage), CellWidth: constCellWidth);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.tempShip.Type = ShipTypes.submarine;
            label1.BorderStyle = BorderStyle.Fixed3D;
            label2.BorderStyle = BorderStyle.None;
            label3.BorderStyle = BorderStyle.None;
            label4.BorderStyle = BorderStyle.None;
        }
        private void label2_Click(object sender, EventArgs e)
        {
            this.tempShip.Type = ShipTypes.destroyer;
            label1.BorderStyle = BorderStyle.None;
            label2.BorderStyle = BorderStyle.Fixed3D;
            label3.BorderStyle = BorderStyle.None;
            label4.BorderStyle = BorderStyle.None;
        }
        private void label3_Click(object sender, EventArgs e)
        {
            this.tempShip.Type = ShipTypes.cruiser;
            label1.BorderStyle = BorderStyle.None;
            label2.BorderStyle = BorderStyle.None;
            label3.BorderStyle = BorderStyle.Fixed3D;
            label4.BorderStyle = BorderStyle.None;
        }
        private void label4_Click(object sender, EventArgs e)
        {
            this.tempShip.Type = ShipTypes.battleship;
            label1.BorderStyle = BorderStyle.None;
            label2.BorderStyle = BorderStyle.None;
            label3.BorderStyle = BorderStyle.None;
            label4.BorderStyle = BorderStyle.Fixed3D;
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            Ship tempShip1 = new Ship(tempShip.Type, tempShip.Gorizontal);
            if (e.Button == MouseButtons.Left)
            {
                tempShip1.X = (e.X - 10) / constCellWidth;
                tempShip1.Y = (e.Y - 10) / constCellWidth;
                //this.Text = string.Format("x = {0}  y= {1}", x, y);

                Rectangle rect;
                if (tempShip1.Gorizontal) rect = new Rectangle(tempShip1.X, tempShip.Y, (int)tempShip1.Type, 1);
                else rect = new Rectangle(tempShip1.X, tempShip1.Y, 1, (int)tempShip1.Type);
                //MessageBox.Show(String.Format("x = {0}  y= {1} w = {2} h {3}", rect.X, rect.Y, rect.Width, rect.Height));

                // Входит в игровое поле кораблик?
                if ((new Rectangle(0, 0, CellCountW, CellCountH)).Contains(rect))
                {
                    // Место свободно / достаточно?
                    bool bFreePlace = true;
                    for (int j = rect.Y - 1; j < rect.Bottom + 1; j++)
                        for (int i = rect.X - 1; i < rect.Right + 1; i++)
                            if ((new Rectangle(0, 0, CellCountW, CellCountH)).Contains(i, j)) // Чтобы не выходить за край массива
                                if (PlayerNavy.Cells[j, i] != CellStatuses.Empty) bFreePlace = false;

                    if (bFreePlace && CountFreeShips[(int)tempShip1.Type - 1] > 0) // Еще есть вакансии на такие корабли
                    {
                        ServiceInstrument.DrawShip(Graphics.FromImage(panel1.BackgroundImage), tempShip1, constCellWidth);
                        panel1.Refresh();
                        switch ((int)tempShip1.Type)
                        {
                            // В case указан размер палубы
                            case 1: --CountFreeShips[0]; label8.Text = string.Format("{0} шт.", CountFreeShips[0]); break;
                            case 2: --CountFreeShips[1]; label9.Text = string.Format("{0} шт.", CountFreeShips[1]); break;
                            case 3: --CountFreeShips[2]; label10.Text = string.Format("{0} шт.", CountFreeShips[2]); break;
                            case 4: --CountFreeShips[3]; label11.Text = string.Format("{0} шт.", CountFreeShips[3]);  break;
                        }
                        ServiceInstrument.InstallShipIntoGrid(this.PlayerNavy.Cells, tempShip1);
                        this.PlayerNavy.Ships[PlayerShipsCounter] = new Ship(tempShip1.X, tempShip1.Y, tempShip1.Type, tempShip1.Gorizontal);
                        PlayerShipsCounter++;
                        if (PlayerShipsCounter == 10)
                            if (MessageBox.Show("Все корабли успешно установлены. \r\n Нажмите 'ОК', чтобы продолжить.", "Завершение установки кораблей", MessageBoxButtons.OK) == DialogResult.OK)
                            {
                                Form1 main = this.Owner as Form1;
                                if (main != null)
                                    main.SetData(this.PlayerNavy);
                                this.Close();                                
                            }
                    }
                }
 
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            int x = (e.X - 10) / constCellWidth;
            int y = (e.Y - 10) / constCellWidth;
            if (tempShip.X != x || tempShip.Y != y)
            {
                tempShip.X = x;
                tempShip.Y = y;
                //this.Text = string.Format("x = {0}  y= {1}", x, y);

                Rectangle rect;
                if (tempShip.Gorizontal) rect = new Rectangle(tempShip.X, tempShip.Y, (int)tempShip.Type, 1);
                else rect = new Rectangle(tempShip.X, tempShip.Y, 1, (int)tempShip.Type);
                //MessageBox.Show(String.Format("x = {0}  y= {1} w = {2} h {3}", rect.X, rect.Y, rect.Width, rect.Height));

                // Входит в игровое поле кораблик?
                if ((new Rectangle(0, 0, CellCountW, CellCountH)).Contains(rect))
                {
                    // Место свободно / достаточно?
                    bool bFreePlace = true;
                    for (int j = rect.Y - 1; j < rect.Bottom + 1; j++)
                        for (int i = rect.X - 1; i < rect.Right + 1; i++)
                            if ((new Rectangle(0, 0, CellCountW, CellCountH)).Contains(i, j)) // Чтобы не выходить за край массива
                                if (PlayerNavy.Cells[j, i] != CellStatuses.Empty) bFreePlace = false;

                    if (bFreePlace && CountFreeShips[(int)tempShip.Type - 1] > 0) // Еще есть вакансии на такие корабли
                    {
                        Graphics g = Graphics.FromImage(panel1.BackgroundImage);
                        for (int j = 0; j < CellCountH; j++)
                            for (int i = 0; i < CellCountW; i++)
                            {
                                Brush br;
                                switch (this.PlayerNavy.Cells[j, i])
                                {
                                    case CellStatuses.Empty:
                                        br = new SolidBrush(Color.White);
                                        break;
                                    case CellStatuses.Mark:
                                        br = new SolidBrush(Color.Red);
                                        break;
                                    case CellStatuses.Ship:
                                        br = new SolidBrush(Color.Yellow);
                                        break;
                                    default:
                                        br = new SolidBrush(Color.White);
                                        break;
                                }
                                g.FillRectangle(br, 11 + i * constCellWidth, 11 + j * constCellWidth, constCellWidth - 1, constCellWidth - 1);
                            }
                        ServiceInstrument.DrawShip(Graphics.FromImage(panel1.BackgroundImage), tempShip, constCellWidth);
                        panel1.Refresh();
                    }
                }
            }
        
        }

        private void panel1_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right) tempShip.Gorizontal = !tempShip.Gorizontal;
        }
    }
}
