﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.IO;


namespace BattleShips_OOP
{
    class Host : IDisposable
    {
        static TcpListener listener = null;
        static Thread thrClientsAdder = null;
        static Client client_enemy = null;

        public bool Active { get { return client_enemy == null ? false : client_enemy.Alive; } }
        private bool active;

        private static List<string> history = new List<string>();

        public event EventHandler<HostStateChangedEventArgs> HostStateChanged;
        private void OnHostStateChanged(HostStateChangedEventArgs e)
        {
            if (HostStateChanged != null)
            {
                EventHandler<HostStateChangedEventArgs> handler = HostStateChanged;
                handler(this, e);
            }
        }
        public class HostStateChangedEventArgs : EventArgs
        {
            public string Message { get { return _message; } }
            string Host { get { return _host; } }
            Int32 Port { get { return _port; } }

            string _message;
            string _host;
            Int32 _port;

            public HostStateChangedEventArgs(string message = "", string host = "", Int32 port = 0)
                : base()
            {
                _message = message;
                _host = host;
                _port = port;
            }
        }

        public Host()
        {
            //active = false;
            client_enemy = new Client();
            client_enemy.MessageRecived += client_enemy_MessageRecived;
        }



        public void Create()
        {
            if (!client_enemy.Alive)
            {
                thrClientsAdder = new Thread(new ThreadStart(AddingNewClients));
                thrClientsAdder.Name = "___New client adder";
                thrClientsAdder.Start();

                OnHostStateChanged(new HostStateChangedEventArgs(message: "Waiting for a connection... . Press ok."));
            }
            else throw new Exception("client_enemy already exist. Возможно сервер уже создан");
        }
        private static void AddingNewClients()
        {
            try
            {
                listener = new TcpListener(IPAddress.Any, 13000);
                listener.Start();

                
                //history.Add("Waiting for a connection... . Press ok.");
                while (true)
                {
                    //Perform a blocking call to accept requests.
                    //You could also user server.AcceptSocket() here.

                    //TcpClient client = listener.AcceptTcpClient();
                    if (listener.Pending() && client_enemy == null)
                    {
                        client_enemy.Assign(listener.AcceptTcpClient());
                        listener.Stop();
                        break;
                    }
                    Thread.Sleep(100);
                }
            }
            catch (SocketException e)
            {
                history.Add(string.Format("SocketException: {0}", e.ToString()));
            }
            finally
            {
                // Stop listening for new clients.
                listener.Stop();
                //clients.ForEach(a => a.Dispose());
            }
        }

        private void client_enemy_MessageRecived(object sender, Host.Client.MessageRecivedEventArgs e)
        {
            OnHostStateChanged(new HostStateChangedEventArgs(message: e.Message));
        }

        public void SendMessage(string message)
        {
            if (client_enemy == null) throw new Exception("Нет пожключённых клиентов!");

            if (!client_enemy.Alive)
                throw new Exception("Соперник не доступен!");

            client_enemy.Send(message);

        }
        
        class Client : IDisposable
        {

            Thread thrProcess;
            TcpClient tcpClient;
            NetworkStream stream;
            DateTime ConnectTime;
            //string data;
            Byte[] bytes = new Byte[256];

            public bool Alive
            {
                get { return tcpClient == null ? false : tcpClient.Connected; }
            }

            int ID;

            public Client() { }
            public void Assign(TcpClient client)
            {
                this.tcpClient = client;
                this.tcpClient.ReceiveTimeout = 300000;


                this.stream = client.GetStream();


                this.thrProcess = new Thread(new ThreadStart(Processing));
                this.ID = thrProcess.ManagedThreadId;
                thrProcess.Start();

                ConnectTime = DateTime.Now;
                OnClientAssigned(new ClientAssignedEventArgs("host test", 0));
                
                
            }

            public event EventHandler<MessageRecivedEventArgs> MessageRecived;
            private void OnMessageRecived(MessageRecivedEventArgs e)
            {
                if (MessageRecived != null)
                {
                    EventHandler<MessageRecivedEventArgs> handler = MessageRecived;
                    handler(this, e);
                }
            }
            public class MessageRecivedEventArgs : EventArgs
            {
                public string Message { get { return _message; } }


                string _message;

                public MessageRecivedEventArgs(string message = "")
                    : base()
                {
                    _message = message;
                }
            }

            public event EventHandler<ClientAssignedEventArgs> ClientAssigned;
            private void OnClientAssigned(ClientAssignedEventArgs e)
            {
                if (MessageRecived != null)
                {
                    EventHandler<ClientAssignedEventArgs> handler = ClientAssigned;
                    handler(this, e);
                }
            }
            public class ClientAssignedEventArgs : EventArgs
            {
                public string Host { get { return _message; } }
                public Int32 Port { get { return _port; } }


                string _message;
                Int32 _port;

                public ClientAssignedEventArgs(string host, Int32 port)
                    : base()
                {
                    _message = host;
                    _port = port;
                }
            }

            

            void Processing()
            {
                int i;
                try
                {
                    // Loop to receive all the data sent by the client.
                    while (tcpClient.Connected)
                    {
                        if ((i = stream.Read(bytes, 0, bytes.Length)) == 0)
                            return;
                        else
                        {
                            // Translate data bytes to a UTF8 string.
                            string data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
                            OnMessageRecived(new MessageRecivedEventArgs(message: data));
                            //Console.WriteLine("Received from <{1}>: {0}", data, this.ID);


                            //Parent.ForEach(x =>
                            //{
                            //    if (x.ID != this.ID)
                            //        x.Send("<" + this.ID.ToString() + ">: " + data);
                            //});

                            //// Process the data sent by the client.
                            //data = data.ToUpper();

                            //byte[] msg = System.Text.Encoding.UTF8.GetBytes(data);

                            //// Send back a response.
                            //stream.Write(msg, 0, msg.Length);
                            //Console.WriteLine("Sent: {0}", data);
                        }
                    }
                }
                catch (IOException e)
                {
                    // Такое исключение получается, когда сервер закрывается.
                    // В одном потоке клиенты вырубаются, а в этом потоке всё ещё прослушиваются.
                    SocketException t = (SocketException)e.InnerException;
                    Console.WriteLine("Closing connection\nError(ID = " + this.ID + ") /n" + e.ToString());
                }
                finally
                {
                    Console.WriteLine("Disconnected! ID = " + this.ID + "(" + this.ConnectTime + ')');
                    Dispose();
                }
            }

            public void Send(string Message)
            {
                byte[] msg = System.Text.Encoding.UTF8.GetBytes(Message);

                // Send back a response.
                stream.Write(msg, 0, msg.Length);
                //Console.WriteLine("SentTo<{1}>:' {0}", Message, this.ID);
            }

            public override string ToString()
            {
                return tcpClient.Client.ToString();
            }

            protected virtual void Dispose(bool disposing)
            {
                if (disposing)
                {
                    if (stream.CanWrite)
                    {
                        Send("Close connection");

                        //this.Alive = false;
                        //thrProcess.Abort();
                        stream.Dispose();
                        tcpClient.Close();

                    }
                }
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }



        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                client_enemy.Dispose();
            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
