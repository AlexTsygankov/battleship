﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.IO;

//using Microsoft.VisualStudio.DebuggerVisualizers;

namespace BattleShips_OOP
{
    public partial class Form_Connection : Form
    {

//        private Host _host = new Host();

        private Client _client = new Client();

        private MyChatHolder chat = null;




        public Form_Connection()
        {
            InitializeComponent();
           chat = new MyChatHolder(ChatBox);
           //_host.HostStateChanged += MessageRecived;
           _client.MessageRecived += _client_MessageRecived;


        }
        delegate void AddToChat(string message);
        void _client_MessageRecived(object sender, Client.MessageRecivedEventArgs e)
        {
            this.BeginInvoke(new AddToChat((mes)=>{ chat.Add(mes); }), e.Message);                       
        }


        private void Create_server_Click(object sender, EventArgs e)
        {
            //_host.Create();
            
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            //host = txtbxaddress.text.replace(',', '.');
            //port = (int32)numericupdown1.value;
            string host = "127.0.0.1";
            Int32 port = 13000;

            if (_client == null)
                _client = new Client();
            _client.ConnectTo(host, port);

        }
        private void button1_Click(object sender, EventArgs e)
        {
            //if (_host != null)
            //    if (_host.Active)
            //        _host.SendMessage(textBox1.Text);

            if(_client != null)
                if(_client.Connected)
                    _client.SendMessage(textBox1.Text);
            //client_enemy.Send();
        }
                
    }

    
}
