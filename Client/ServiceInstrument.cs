﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace BattleShips_OOP
{
    public abstract class ServiceInstrument
    {

        /// <summary>
        /// Рисует сетку указанным объектом Graphics
        /// </summary>
        /// <param name="graph"> Чем и где рисовать</param>
        /// <param name="collumns"> Кол-во колонок [по умолчанию 10]</param>
        /// <param name="rows"> Кол-во строк [по умолчанию 10]</param>
        /// <param name="CellWidth"> Ширина одной клетки в пикселях [по умолчанию 20]</param>
        /// <param name="CellHeight"> Высота одной клетки в пикселях [по умолчанию 20]</param>
        public static void DrawGrid(Graphics graph, int collumns = 10, int rows = 10, int CellWidth = 20, int CellHeight = 20)
        {
            char ch = 'А'; // Для горизонтальных координат поля
            for (int i = 0; i < collumns; i++)
            {
                graph.DrawString(string.Format("{0}", ch), new Font("Times new Roman", 8),
                                    new SolidBrush(Color.Black), CellWidth * i + 10, -3);
                ch++;
                if (ch == 'Й') ch++;
            }

            // Вертикальные линии поля
            for (int i = 0; i < rows; i++)
                graph.DrawString(string.Format("{0}", i + 1), new Font("Times new Roman", 8),
                                    new SolidBrush(Color.Black), -3, CellWidth * i + 10);

            // Горизонтальные линии
            for (int i = 0; i < collumns + 1; i++)
                graph.DrawLine(new Pen(new SolidBrush(Color.Black)), 10, CellWidth * i + 10,
                                collumns * CellWidth + 10, CellWidth * i + 10);

            // Вертикальные линии
            for (int i = 0; i < rows + 1; i++)
                graph.DrawLine(new Pen(new SolidBrush(Color.Black)), CellWidth * i + 10,
                                    10, CellWidth * i + 10, rows * CellWidth + 10);

            graph.Dispose();
        }
        public static void DrawShip(Graphics gr, Ship DrawingShip, int CellWidth = 20, bool Mark = true)
        {
            Rectangle rect;
            if (DrawingShip.Gorizontal)
                rect = new Rectangle(DrawingShip.X, DrawingShip.Y, (int)DrawingShip.Type, 1);
            else
                rect = new Rectangle(DrawingShip.X, DrawingShip.Y, 1, (int)DrawingShip.Type);

            if (Mark)
            {
                // Подсвечивание занятого места
                for (int j = rect.Y - 1; j < rect.Bottom + 1; j++)
                    for (int i = rect.X - 1; i < rect.Right + 1; i++)
                        if ((new Rectangle(0, 0, 10, 10)).Contains(i, j))
                            gr.FillRectangle(new SolidBrush(Color.Red), 11 + i * CellWidth, 11 + j * CellWidth, CellWidth - 1, CellWidth - 1);
            }
            // Установка корабля
            for (int j = rect.Y - 1; j < rect.Bottom + 1; j++)
                for (int i = rect.X - 1; i < rect.Right + 1; i++)
                    if (rect.Contains(i, j))
                        gr.FillRectangle(new SolidBrush(Color.Yellow), 11 + i * CellWidth, 11 + j * CellWidth, CellWidth - 1, CellWidth - 1);

            gr.Dispose();
        }
        public static void InstallShipIntoGrid(CellStatuses[,] array, Ship InstallingShip)
        {
            Rectangle rect;
            if (InstallingShip.Gorizontal)
                rect = new Rectangle(InstallingShip.X, InstallingShip.Y, (int)InstallingShip.Type, 1);
            else
                rect = new Rectangle(InstallingShip.X, InstallingShip.Y, 1, (int)InstallingShip.Type);

            // Установка корабля
            for (int j = rect.Y - 1; j < rect.Bottom + 1; j++)
                for (int i = rect.X - 1; i < rect.Right + 1; i++)
                    if (rect.Contains(i, j))
                        array[j, i] = CellStatuses.Ship; // shipBlocks
        }
        public static NavyGrid CreateComputerNavy()
        {
            Random rand = new Random();
            int count = 0;
            int CellCount = 10;
            int[] CountFreeShips = new int[4];
            CountFreeShips[0] = 4;
            CountFreeShips[1] = 3;
            CountFreeShips[2] = 2;
            CountFreeShips[3] = 1;
            NavyGrid tempNavi = new NavyGrid();

            while (count < 10)
            {
                Ship tempShip = new Ship(ShipTypes.battleship, true);
                tempShip.X = rand.Next(0, 10);
                tempShip.Y = rand.Next(0, 10);

                tempShip.Type = (ShipTypes)rand.Next(1, 5);
                Rectangle rect;
                if (rand.Next(2) == 1)
                {
                    tempShip.Gorizontal = true;
                    rect = new Rectangle(tempShip.X, tempShip.Y, (int)tempShip.Type, 1);
                }
                else
                {
                    tempShip.Gorizontal = false;
                    rect = new Rectangle(tempShip.X, tempShip.Y, 1, (int)tempShip.Type);
                }

                // Входит в игровое поле кораблик?
                if ((new Rectangle(0, 0, CellCount, CellCount)).Contains(rect))
                {
                    // Место свободно / достаточно?
                    bool FreePlace = true;
                    for (int j = rect.Y - 1; j < rect.Bottom + 1; j++)
                        for (int i = rect.X - 1; i < rect.Right + 1; i++)
                            if (i < CellCount && i > -1 && j > -1 && j < CellCount) // Чтобы не выходить за край массива
                                if (tempNavi.Cells[j, i] != 0) FreePlace = false;

                    if (FreePlace && CountFreeShips[(int)tempShip.Type - 1] > 0) // Еще есть вакансии на такие корабли&
                    {
                        for (int j = rect.Y - 1; j < rect.Bottom + 1; j++)
                            for (int i = rect.X - 1; i < rect.Right + 1; i++)
                                if (rect.Contains(i, j)) tempNavi.Cells[j, i] = CellStatuses.Ship; // shipBlocks
                        switch (tempShip.Type)
                        {
                            // В case указан размер палубы
                            case ShipTypes.submarine: --CountFreeShips[0];

                                break;
                            case ShipTypes.destroyer: --CountFreeShips[1];
                                break;
                            case ShipTypes.cruiser: --CountFreeShips[2];
                                break;
                            case ShipTypes.battleship: --CountFreeShips[3];
                                break;
                        }
                        tempNavi.Ships[count] = new Ship(tempShip.X, tempShip.Y, tempShip.Type, tempShip.Gorizontal);
                        count++;
                    }
                }
            }
            return tempNavi;
        }
        /// <summary>
        /// Помечает графически зону вокруг корабля
        /// </summary>
        /// <param name="gr1"> Место вывода/рисования</param>
        /// <param name="rect"> Зона корабля</param>
        public static void MarkAreaAroudBrokenShip(Graphics gr, Rectangle rect, int CellWidth, CellStatuses[,] cellstat)
        {
            Rectangle rc = new Rectangle(rect.X + 1, rect.Y + 1, rect.Width - 2, rect.Height - 2);
            for (int j = 0; j < 10; j++)
                for (int i = 0; i < 10; i++)
                    if (rect.Contains(i, j) && !rc.Contains(i, j))
                    {
                        gr.DrawEllipse(new Pen(new SolidBrush(Color.Black)), 14 + i * CellWidth, 14 + j * CellWidth, CellWidth - 6, CellWidth - 6);
                        //gr1.DrawEllipse(new Pen(new SolidBrush(Color.Red)), 11 + i * CellWidth, 11 + j * CellWidth, 11 + i * 2 * CellWidth, 11 + j * 2 * CellWidth);
                        cellstat[j, i] = CellStatuses.Miss;
                    }
        }
        public static List<Point> assambleAvailablePtVector(ref NavyGrid comp, ref NavyGrid player)
        {
            List<Point> listAvailablePoints = new List<Point>();

            if (comp.lastHitStat == HitStatus.Kill ||
                (comp.lastHitStat == HitStatus.Miss && comp.lastHitPoint.X == -1 && comp.lastHitPoint.Y == -1)) // Такое, когда промазал дважды либо старт
            {
                // Собираем для хода все свободные ячейки
                for (int i = 0; i < 10; i++)
                    for (int j = 0; j < 10; j++)
                        if (player.Cells[i, j] == CellStatuses.Empty ||
                            player.Cells[i, j] == CellStatuses.Ship)
                            listAvailablePoints.Add(new Point(j, i));
                return listAvailablePoints;
            }
            else // Когда промазал либо ранен
            {
                Point lastDestrPart = new Point(comp.lastHitPoint.X, comp.lastHitPoint.Y);

                // Слева
                for (int i = 0; i < 4; i++)
                {
                    lastDestrPart.X -= 1;
                    if (lastDestrPart.X >= 0 && lastDestrPart.X < 10)
                    {
                        if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Empty ||
                            player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Ship)
                        {
                            listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            break;
                        }
                        // Посмотреть/подобрать ячейки слева и справа
                        else if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.DestroyedPart)
                        {
                            int bufCoord = lastDestrPart.Y + 1;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Empty ||
                                    player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            bufCoord -= 2;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Empty ||
                                    player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                        }
                        else break;
                    }
                }

                // Сверху
                lastDestrPart.X = comp.lastHitPoint.X;
                lastDestrPart.Y = comp.lastHitPoint.Y;
                for (int i = 0; i < 4; i++)
                {
                    lastDestrPart.Y -= 1;
                    if (lastDestrPart.Y >= 0 && lastDestrPart.Y < 10)
                        if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Empty ||
                            player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Ship)
                        {
                            listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            break;
                        }
                        else if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.DestroyedPart)
                        {
                            int bufCoord = lastDestrPart.X + 1;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Empty ||
                                    player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            bufCoord -= 2;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Empty ||
                                    player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                        }
                        else break;
                }

                // Справа
                lastDestrPart.X = comp.lastHitPoint.X;
                lastDestrPart.Y = comp.lastHitPoint.Y;
                for (int i = 0; i < 4; i++)
                {
                    lastDestrPart.X += 1;
                    if (lastDestrPart.X >= 0 && lastDestrPart.X < 10)
                        if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Empty ||
                            player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Ship)
                        {
                            listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            break;
                        }
                        else if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.DestroyedPart)
                        {
                            int bufCoord = lastDestrPart.Y + 1;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Empty ||
                                    player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            bufCoord -= 2;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Empty ||
                                    player.Cells[bufCoord, lastDestrPart.X] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                        }
                        else break;
                }

                // Снизу
                lastDestrPart.X = comp.lastHitPoint.X;
                lastDestrPart.Y = comp.lastHitPoint.Y;
                for (int i = 0; i < 4; i++)
                {
                    lastDestrPart.Y += 1;
                    if (lastDestrPart.Y >= 0 && lastDestrPart.Y < 10)
                        if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Empty ||
                            player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.Ship)
                        {
                            listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            break;
                        }
                        else if (player.Cells[lastDestrPart.Y, lastDestrPart.X] == CellStatuses.DestroyedPart)
                        {
                            int bufCoord = lastDestrPart.X + 1;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Empty ||
                                    player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                            bufCoord -= 2;
                            if (bufCoord >= 0 && bufCoord < 10)
                                if (player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Empty ||
                                    player.Cells[lastDestrPart.Y, bufCoord] == CellStatuses.Ship)
                                    listAvailablePoints.Add(new Point(lastDestrPart.X, lastDestrPart.Y));
                        }
                        else break;
                }
                return listAvailablePoints;
            }// end else
        }// methode assamble...
    } // class
}
