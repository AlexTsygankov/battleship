﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using System.Net;
using System.Net.Sockets;
using System.IO;

using System.Windows.Forms;

namespace BattleShips_OOP
{
    class Client : IDisposable {

        private static TcpClient tcpClient = null;
        private static NetworkStream stream = null;
        private static Thread thrListener = null;
        //private static List<string> chat = null;
        private static string _host;
        private static Int32 _port;

        public bool Connected { get { return tcpClient == null ? false : tcpClient.Connected; } }
        private static bool connected = false;

        public void ConnectTo(string host, Int32 port )
        {
            _host = host;
            _port = port;
           (new Thread(new ThreadStart(ConnectToHost))).Start();
            
        }
        private void ConnectToHost()
        {

            try
            {
                //MessageBox.Show("Connecting... \n");
                thrListener = new Thread(ListenServer);
                thrListener.Name = "____ListenServer____";

                tcpClient = new TcpClient(_host, _port);
                stream = tcpClient.GetStream();

                // здесь нужен handshake и распределение по комнатам


                thrListener.Start();

                OnConnected(new EventArgs());
                //MessageBox.Show("Connected!\n");
            }
            catch (ArgumentNullException d)
            {
                MessageBox.Show("ArgumentNullException: " + d.Message + '\n');
                //ConnectionFailure();
            }
            catch (SocketException d)
            {
                MessageBox.Show("SocketException: " + d.Message + '\n');
                //ConnectionFailure();
            }
        }
        private void ListenServer(/*object sender, EventArgs e*/)
        {
            while (tcpClient.Connected)
            {
                if (stream.CanRead)
                {
                    // Buffer for reading data
                    Byte[] bytes = new Byte[256];
                    String data = null;
                    int i = 0;

                    try
                    {
                        // Loop to receive all the data sent by the client.
                        while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {

                            // Translate data bytes to a UTF8 string.
                            data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
                            //MessageBox.Show("server: " + data + '\n');
                            OnMessageRecived(new MessageRecivedEventArgs(data));
                        }


                    }
                    catch (IOException e)
                    {
                        if (e.InnerException is SocketException)
                        {
                            MessageBox.Show(((SocketException)e.InnerException).ErrorCode + ":  " + e.InnerException.Message);
                        }
                        //else
                        MessageBox.Show("Time out(" + ") /n" + e.Message);
                    }
                    finally
                    {
                        //btnDisconnect_Click(btnDisconnect, new EventArgs());
                        //MessageBox.Show("Disconnected!(" + ')');
                        //if (data == "Close connection")
                        //{
                        //    MessageBox.Show("Disconnected!");
                        //}
                        tcpClient.Close();
                    }
                }
            }
        }
        public void SendMessage(string message)
        {
            if (tcpClient != null && tcpClient.Connected)
            {
                //string message = "TESTmessage";
                //MessageBox.Show("you: " + message + '\n');
                // Translate the passed message into UTF8 and store it as a Byte array.
                Byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length);
                //tbxSender.Text = "";
            }
        }

        public event EventHandler<MessageRecivedEventArgs> MessageRecived;
        private void OnMessageRecived(MessageRecivedEventArgs e)
        {
            if (MessageRecived != null)
            {
                EventHandler<MessageRecivedEventArgs> handler = MessageRecived;
                handler(this, e);
            }
        }
        public class MessageRecivedEventArgs : EventArgs
        {
            public string Message { get { return _message; } }


            string _message;

            public MessageRecivedEventArgs(string message = "")
                : base()
            {
                _message = message;
            }
        }
        
        public event EventHandler ConnectedToServer;
        private void OnConnected(EventArgs e)
        {
            if (ConnectedToServer != null)
            {
                EventHandler handler = ConnectedToServer;
                handler(this, e);
            }
        }


        public override string ToString()
        {
            return
            "RemovePoint(" + tcpClient.Client.RemoteEndPoint.ToString() +
            ") LocalPoint(" + tcpClient.Client.LocalEndPoint.ToString() + ")";
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                //stream.Dispose();
                tcpClient.Close();

            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
    }
}
