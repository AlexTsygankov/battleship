﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Media;
using System.Collections.Generic;
using System.Windows;




namespace BattleShips_OOP
{
    public partial class Form1 : Form
    {
        private NavyGrid Player;
        private NavyGrid Computer;
        private NavyGrid Enemy;

        private const int constCellWidth = 30;
        private bool PlayerTurn = true;
        private bool CheckSound = true;
        private bool DataSetted = false;

        private bool NetGame = false;
        private Client _client = new Client();

        private MyChatHolder chat = null;


        public Form1()
        {
            InitializeComponent();
            panel1.BackgroundImage = new Bitmap(panel1.Width, panel1.Height);
            panel2.BackgroundImage = new Bitmap(panel2.Width, panel2.Height);

            chat = new MyChatHolder(listBox1);
            //_host.HostStateChanged += MessageRecived;
            _client.MessageRecived += _client_MessageRecived;
            _client.ConnectedToServer += _client_ConnectedToServer;
        }

        private void _client_ConnectedToServer(object sender, EventArgs e)
        {
            this.BeginInvoke(new Action(() => { chat.Add(string.Format("Connected to {0}", _client.ToString())); }));
            if (Player != null)
                _client.SendMessage(string.Format("{0} {1} {2} {3}",
                    BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ask],
                    BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game],
                    BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships],
                    Player.ToString()));
        }

        private void _client_MessageRecived(object sender, Client.MessageRecivedEventArgs e)
        {
            this.BeginInvoke(new Action<string>((mes) =>
            {
                chat.Add(mes);
                ParseRecivedMessages(mes);
            }), e.Message);
        }
        private void ParseRecivedMessages(string message)
        {
            string[] parts = message.Split(" :".ToCharArray());

            if (parts[0] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ask])
            {
                if (parts[1] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.shoot])
                {
                    int x, y;
                    if (int.TryParse(parts[2], out x) &&
                        int.TryParse(parts[3], out y))
                    {
                        // shoot to my zone
                        Point shot = new Point(x, y);
                        Graphics gr = Graphics.FromImage(panel1.BackgroundImage);
                        System.Media.SoundPlayer SoundPlayer = new SoundPlayer(BattleShips_OOP.Properties.Resources.gunFire);
                        if (CheckSound) SoundPlayer.Play();

                        // Проверка и отрисовка
                        if (Player.Cells[shot.Y, shot.X] == CellStatuses.Ship) // корабль
                        {
                            SoundPlayer PL = new SoundPlayer(BattleShips_OOP.Properties.Resources.Explosion);
                            if (CheckSound) PL.Play();
                            Player.Cells[shot.Y, shot.X] = CellStatuses.DestroyedPart;

                            // Нарисовать крест на палубе корабля
                            gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + shot.X * constCellWidth, 11 + shot.Y * constCellWidth,
                                9 + shot.X * constCellWidth + constCellWidth, 9 + constCellWidth + shot.Y * constCellWidth);
                            gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + shot.X * constCellWidth, 9 + constCellWidth + shot.Y * constCellWidth,
                                9 + shot.X * constCellWidth + constCellWidth, 11 + shot.Y * constCellWidth);

                            Computer.lastHitStat = HitStatus.Hit;
                            Computer.setLastHitPoint(shot.X, shot.Y);

                            // Найти корабль и покоцать его
                            foreach (Ship SP in Player.Ships)
                                if (SP.Contains(shot.X, shot.Y))
                                {
                                    --SP.Health;
                                    // Обработать полное уничтожение корабля
                                    if (SP.Health == 0)
                                    {
                                        if (CheckSound) PL.Play();
                                        Rectangle rect;
                                        if (SP.Gorizontal)
                                            rect = new Rectangle(SP.X - 1, SP.Y - 1, (int)SP.Type + 2, 3);
                                        else
                                            rect = new Rectangle(SP.X - 1, SP.Y - 1, 3, (int)SP.Type + 2);
                                        ServiceInstrument.MarkAreaAroudBrokenShip(gr, rect, constCellWidth, Player.Cells);

                                        Computer.lastHitStat = HitStatus.Kill;
                                        Computer.setLastHitPoint(-1, -1);
                                    }
                                    break;
                                }
                            PlayerTurn = false;  // Продолжение хода  
                        }
                        else if (Player.Cells[shot.Y, shot.X] == CellStatuses.Empty) // вода или занято
                        {
                            Player.Cells[shot.Y, shot.X] = CellStatuses.Miss;

                            // !-- Припечание : звук "плюх"(попадание в воду) похоже не найден. Не запилил короче.

                            gr.DrawEllipse(new Pen(new SolidBrush(Color.Black)), 14 + shot.X * constCellWidth,
                                14 + shot.Y * constCellWidth, constCellWidth - 6, constCellWidth - 6);
                            PlayerTurn = true; // Переход хода

                            Computer.lastHitStat = HitStatus.Miss;

                            Computer.setLastHitPoint(x, y);
                        }


                        if (!GameLogic.CheckEndGameCondition(Player.Ships, Computer.Ships, ref this.PlayerTurn))
                        {
                            if (!PlayerTurn)
                            {   // ~Рекурсия. Повтор хода компьютера.
                                toolStripLabel1.Text = "Ход противника... ";
                                //timer1.Start();
                            }
                            else toolStripLabel1.Text = "Твой ход";
                        }
                        panel1.Refresh();//      BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.miss]));
                    }
                }
                if (parts[1] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships])
                {
                    _client.SendMessage(string.Format("{0} {1} {2}", BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer],
                        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships],
                        Player.ToString()));
                }
            }
            else if (parts[0] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer])
            {
                if (parts[1] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game])
                {
                    if (parts[2] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships])
                    {
                        if (Computer == null)
                            Computer = new NavyGrid();
                        this.Computer.FromString(parts[3]);
                    }
                }
                else if (parts[1] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.shoot])
                {
                    bool _playerTurn;
                    if (bool.TryParse(parts[2], out _playerTurn))
                    {
                        this.PlayerTurn = _playerTurn;
                    }
                }
            }
        }

        private void новаяИграToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!_client.Connected)
                NewGameLocale();
            else
            {
                NewNetGame();
                if (_client.Connected)
                    _client.SendMessage(string.Format("{0} {1} {2} {3}",
                        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ask],
                        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game],
                        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships],
                        Player.ToString()));

                //debug
                //if (Computer == null)
                //    Computer = new NavyGrid();
                //this.Computer.FromString(Player.ToString());
            }
        }

        private void NewNetGame()
        {
            Graphics g = Graphics.FromImage(panel1.BackgroundImage);
            g.Clear(Color.White);
            g = Graphics.FromImage(panel2.BackgroundImage);
            g.Clear(Color.White);

            label1.Visible = false;
            label2.Visible = false;
            panel1.Visible = false;
            panel2.Visible = false;
            toolStripLabel1.Visible = false;

            Form_CeatePlayerNavy form = new Form_CeatePlayerNavy();
            form.Owner = this;
            form.ShowDialog();

            if (this.DataSetted)
            {

                label1.Visible = true;
                label2.Visible = true;
                panel1.Visible = true;
                panel2.Visible = true;
                toolStripLabel1.Visible = true;

                this.PlayerTurn = true;
                toolStripLabel1.Text = "Твой ход";

                ServiceInstrument.DrawGrid(Graphics.FromImage(panel1.BackgroundImage), CellHeight: 30, CellWidth: 30);
                ServiceInstrument.DrawGrid(Graphics.FromImage(panel2.BackgroundImage), CellHeight: 30, CellWidth: 30);

                foreach (Ship ship in Player.Ships)
                    ServiceInstrument.DrawShip(Graphics.FromImage(panel1.BackgroundImage), ship, CellWidth: constCellWidth, Mark: false);

                panel1.Refresh();
                panel2.Refresh();
            }
        }

        private void NewGameLocale()
        {
            Graphics g = Graphics.FromImage(panel1.BackgroundImage);
            g.Clear(Color.White);
            g = Graphics.FromImage(panel2.BackgroundImage);
            g.Clear(Color.White);

            label1.Visible = false;
            label2.Visible = false;
            panel1.Visible = false;
            panel2.Visible = false;
            toolStripLabel1.Visible = false;


            Form_CeatePlayerNavy form = new Form_CeatePlayerNavy();
            form.Owner = this;
            form.ShowDialog();

            if (this.DataSetted)
            {

                this.Computer = ServiceInstrument.CreateComputerNavy();

                label1.Visible = true;
                label2.Visible = true;
                panel1.Visible = true;
                panel2.Visible = true;
                toolStripLabel1.Visible = true;

                this.PlayerTurn = true;
                toolStripLabel1.Text = "Твой ход";

                ServiceInstrument.DrawGrid(Graphics.FromImage(panel1.BackgroundImage), CellHeight: 30, CellWidth: 30);
                ServiceInstrument.DrawGrid(Graphics.FromImage(panel2.BackgroundImage), CellHeight: 30, CellWidth: 30);

                foreach (Ship ship in Player.Ships)
                    ServiceInstrument.DrawShip(Graphics.FromImage(panel1.BackgroundImage), ship, CellWidth: constCellWidth, Mark: false);

                panel1.Refresh();
                panel2.Refresh();
            }
            //else { client_enemy.Dispose(); }
        }
        public void SetData(NavyGrid player)
        {
            this.Player = player;
            this.DataSetted = true;
        }
        private void panel2_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && PlayerTurn) // ЛКМ и Ход игрока ?
            {
                int x = (e.X - 10) / constCellWidth;
                int y = (e.Y - 10) / constCellWidth;
                if (Computer != null && Player != null)
                {
                    if ((new Rectangle(0, 0, 10, 10)).Contains(x, y)) // Клик внутри поля?
                    {
                        if (_client.Connected)
                            SendShoot(x, y);
                        ProcessPlayerShoot(x, y);
                    }   //endif клик внутри поля

                    if (!GameLogic.CheckEndGameCondition(this.Player.Ships, this.Computer.Ships, ref this.PlayerTurn))
                    {
                        if (!PlayerTurn)
                        {
                            toolStripLabel1.Text = "Ход противника... ";
                            if (!_client.Connected)
                                timer1.Start();
                        }
                        else toolStripLabel1.Text = "Твой ход";
                    }
                    panel2.Refresh();
                }
            }
        }
        private void ProcessPlayerShoot(int x, int y)
        {

            if (Computer.Cells[y, x] != CellStatuses.Miss
                        && Computer.Cells[y, x] != CellStatuses.DestroyedPart) // Эта ячейка уже была использована? пропустить
            {
                System.Media.SoundPlayer Player = new SoundPlayer(BattleShips_OOP.Properties.Resources.gunFire);
                if (CheckSound) Player.Play();
                Graphics gr = Graphics.FromImage(panel2.BackgroundImage);

                // Состояние клетки: корабль
                if (Computer.Cells[y, x] == CellStatuses.Ship)
                {
                    SoundPlayer PL = new SoundPlayer(BattleShips_OOP.Properties.Resources.Explosion);
                    if (CheckSound) PL.Play();

                    Computer.Cells[y, x] = CellStatuses.DestroyedPart;     // Состояние: Ранен
                    // Найти корабль по координатам
                    foreach (Ship SP in Computer.Ships)
                        if (SP.Contains(x, y))
                        {
                            --SP.Health;
                            if (SP.Health == 0) // Убит?
                            {
                                if (CheckSound) PL.Play();
                                Rectangle rect;
                                if (SP.Gorizontal) rect = new Rectangle(SP.X - 1, SP.Y - 1, (int)SP.Type + 2, 3);
                                else rect = new Rectangle(SP.X - 1, SP.Y - 1, 3, (int)SP.Type + 2);
                                ServiceInstrument.MarkAreaAroudBrokenShip(gr, rect, constCellWidth, Computer.Cells);
                            }
                        }
                    gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + x * constCellWidth, 11 + y * constCellWidth,
                        9 + x * constCellWidth + constCellWidth, 9 + constCellWidth + y * constCellWidth);
                    gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + x * constCellWidth, 9 + constCellWidth + y * constCellWidth,
                        9 + x * constCellWidth + constCellWidth, 11 + y * constCellWidth);

                    PlayerTurn = true;  // Продолжение хода
                }
                else if (Computer.Cells[y, x] == CellStatuses.Empty
                        || Computer.Cells[y, x] == CellStatuses.Mark) // Вода или Занято
                {
                    Computer.Cells[y, x] = CellStatuses.Miss;
                    gr.DrawEllipse(new Pen(new SolidBrush(Color.Black)), 14 + x * constCellWidth,
                        14 + y * constCellWidth, constCellWidth - 6, constCellWidth - 6);
                    PlayerTurn = false; // Конец хода
                }
            }
        }
        private void SendShoot(int x, int y)
        {
            _client.SendMessage(string.Format("ask shoot {0}:{1}", x, y));
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromImage(panel2.BackgroundImage);
            for (int j = 0; j < 10; j++)
                for (int i = 0; i < 10; i++)
                    if (Computer.Cells[j, i] == CellStatuses.Ship)
                        g.FillRectangle(new SolidBrush(Color.Yellow), 11 + i * constCellWidth,
                                        11 + j * constCellWidth, constCellWidth, constCellWidth);
            panel2.Refresh();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();

            /******************** Ход компьютера *****************/
            // Вектор свободных ходов
            List<Point> vector = ServiceInstrument.assambleAvailablePtVector(ref Computer, ref Player);

            if (vector.Count == 0)
            {
                Computer.setLastHitPoint(-1, -1);
                Computer.lastHitStat = HitStatus.Miss;
                vector = ServiceInstrument.assambleAvailablePtVector(ref Computer, ref Player);
            }

            Random rand = new Random();
            Point shot = vector[rand.Next(vector.Count)];

            Graphics gr = Graphics.FromImage(panel1.BackgroundImage);
            System.Media.SoundPlayer SoundPlayer = new SoundPlayer(BattleShips_OOP.Properties.Resources.gunFire);
            if (CheckSound) SoundPlayer.Play();

            // Проверка и отрисовка
            if (Player.Cells[shot.Y, shot.X] == CellStatuses.Ship) // корабль
            {
                SoundPlayer PL = new SoundPlayer(BattleShips_OOP.Properties.Resources.Explosion);
                if (CheckSound) PL.Play();
                Player.Cells[shot.Y, shot.X] = CellStatuses.DestroyedPart;

                // Нарисовать крест на палубе корабля
                gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + shot.X * constCellWidth, 11 + shot.Y * constCellWidth,
                    9 + shot.X * constCellWidth + constCellWidth, 9 + constCellWidth + shot.Y * constCellWidth);
                gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + shot.X * constCellWidth, 9 + constCellWidth + shot.Y * constCellWidth,
                    9 + shot.X * constCellWidth + constCellWidth, 11 + shot.Y * constCellWidth);

                Computer.lastHitStat = HitStatus.Hit;
                Computer.setLastHitPoint(shot.X, shot.Y);

                // Найти корабль и покоцать его
                foreach (Ship SP in Player.Ships)
                    if (SP.Contains(shot.X, shot.Y))
                    {
                        --SP.Health;
                        // Обработать полное уничтожение корабля
                        if (SP.Health == 0)
                        {
                            if (CheckSound) PL.Play();
                            Rectangle rect;
                            if (SP.Gorizontal)
                                rect = new Rectangle(SP.X - 1, SP.Y - 1, (int)SP.Type + 2, 3);
                            else
                                rect = new Rectangle(SP.X - 1, SP.Y - 1, 3, (int)SP.Type + 2);
                            ServiceInstrument.MarkAreaAroudBrokenShip(gr, rect, constCellWidth, Player.Cells);

                            Computer.lastHitStat = HitStatus.Kill;
                            Computer.setLastHitPoint(-1, -1);
                        }
                        break;
                    }
                PlayerTurn = false;  // Продолжение хода  
            }
            else if (Player.Cells[shot.Y, shot.X] == CellStatuses.Empty) // вода или занято
            {
                Player.Cells[shot.Y, shot.X] = CellStatuses.Miss;

                // !-- Припечание : звук "плюх"(попадание в воду) похоже не найден. Не запилил короче.

                gr.DrawEllipse(new Pen(new SolidBrush(Color.Black)), 14 + shot.X * constCellWidth,
                    14 + shot.Y * constCellWidth, constCellWidth - 6, constCellWidth - 6);
                PlayerTurn = true; // Переход хода

                Computer.lastHitStat = HitStatus.Miss;

                // Рандомно решаю, продолжить обстрел корабля или попытать счастья в открытом поле
                if (rand.Next(1) == -1 && Computer.lastHitPoint.X != -1)
                    Computer.setLastHitPoint(-1, -1);
            }


            if (!GameLogic.CheckEndGameCondition(Player.Ships, Computer.Ships, ref this.PlayerTurn))
            {
                if (!PlayerTurn)
                {   // ~Рекурсия. Повтор хода компьютера.
                    toolStripLabel1.Text = "Ход компьютера... ";
                    timer1.Start();
                }
                else toolStripLabel1.Text = "Твой ход";
            }
            panel1.Refresh();
            vector.Clear();
        }

        private void PauseTimer_Tick(object sender, EventArgs e)
        {
            PauseTimer.Stop();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            CheckSound = !CheckSound;
            if (CheckSound)
                label3.Image = BattleShips_OOP.Properties.Resources.sound;
            else
                label3.Image = BattleShips_OOP.Properties.Resources.sound2;
        }

        private void создатьСерверToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }


        private void подключитьсяToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string host = txbxHost.Text;
            Int32 port = (Int32)nmPort.Value;

            if (_client == null)
                _client = new Client();
            _client.ConnectTo(host, port);


            //NewGame();
        }

    }


}


//////////////////////////////////
// Старая версия ИИ
///
/*///*******************Ход робота*******************/
//Random rand = new Random();
//int x = rand.Next(0, 10);
//int y = rand.Next(0, 10);
//bool flagRepeat = true;

//while (flagRepeat)
//{
//    if (Player.Cells[y, x] != CellStatuses.DestroyedPart 
//        || Player.Cells[y, x] != CellStatuses.Miss) // Эта ячейка уже была использована, пропустить
//    {
//        flagRepeat = false;
//        Graphics gr = Graphics.FromImage(panel1.BackgroundImage);
//        System.Media.SoundPlayer SoundPlayer = new SoundPlayer(BattleShips_OOP.Properties.Resources.gunFire);
//        if (CheckSound) SoundPlayer.Play();

//        if (Player.Cells[y, x] == CellStatuses.Ship) // корабль
//        {
//            SoundPlayer PL = new SoundPlayer(BattleShips_OOP.Properties.Resources.Explosion);
//            if (CheckSound) PL.Play();
//            Player.Cells[y, x] = CellStatuses.DestroyedPart;
//            foreach (Ship SP in Player.Ships)
//                if (SP.Contains(x, y))
//                {
//                    --SP.Health;
//                    if (SP.Health == 0)
//                    {
//                        if (CheckSound) PL.Play(); 
//                        Rectangle rect;
//                        if (SP.Gorizontal)
//                            rect = new Rectangle(SP.X - 1, SP.Y - 1, (int)SP.Type + 2, 3);
//                        else 
//                            rect = new Rectangle(SP.X - 1, SP.Y - 1, 3, (int)SP.Type + 2);
//                        ServiceInstrument.MarkAreaAroudBrokenShip(gr, rect, constCellWidth, Player.Cells);
//                    }
//                }
//            gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + x * constCellWidth, 11 + y * constCellWidth,
//                9 + x * constCellWidth + constCellWidth, 9 + constCellWidth + y * constCellWidth);
//            gr.DrawLine(new Pen(new SolidBrush(Color.Red)), 11 + x * constCellWidth, 9 + constCellWidth + y * constCellWidth,
//                9 + x * constCellWidth + constCellWidth, 11 + y * constCellWidth);
//            PlayerTurn = false;  // Продолжение хода                      
//        }
//        else if (Player.Cells[y, x] == CellStatuses.Empty 
//            || Player.Cells[y, x] == CellStatuses.Mark) // вода или занято
//        {
//            Player.Cells[y, x] = CellStatuses.Miss;
//            gr.DrawEllipse(new Pen(new SolidBrush(Color.Black)), 14 + x * constCellWidth,
//                14 + y * constCellWidth, constCellWidth - 6, constCellWidth - 6);
//            PlayerTurn = true; // Переход хода
//        }
//    } // end if
//} // end While*/