﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace BattleShips_OOP
{
    class GameLogic
    {
        /// <summary>
        /// Проверка условий победы/продолжения игры
        /// </summary>
        /// <returns> true = конец игры</returns>
        /// <param name="playerTurn"> Замочек на клики после завершения игры </param>  
        public static bool CheckEndGameCondition(Ship[] PlayerShips, Ship[] ComputerShips, ref bool playerTurn)
        {

            bool PlayerNavyAlive = false;
            bool ComputerNavyAlive = false;

            foreach (Ship SP in PlayerShips)
                if (SP.Health > 0) PlayerNavyAlive = true;
            foreach (Ship SP in ComputerShips)
                if (SP.Health > 0) ComputerNavyAlive = true;
            
            if (ComputerNavyAlive && PlayerNavyAlive) return false; // не конец игры
            else if (PlayerNavyAlive)
            {
                MessageBox.Show("Вы победитель! Поздравляю!");
                playerTurn = false; // Замочек на клики после завершения игры
            }
            else if (ComputerNavyAlive) // Я знаю, что излишне
            {
                MessageBox.Show("Победил компьютер. В другой раз повезет и вам.");
                playerTurn = false; // Замочек на клики после завершения игры
            }
            return true;
        }
    }
}
