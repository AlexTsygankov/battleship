﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using BtSh_Model;

namespace BattleShips_WPF
{
    /// <summary>
    /// Логика взаимодействия для Constructor.xaml
    /// </summary>
    public partial class Constructor : Window 
    {

        private  ShipsHolder MyNavy
        {
            get { return (ShipsHolder)Resources["ShipsHolderResource"]; }
        }

        private ShipTemplate MyTempShip
        {
            get { return (ShipTemplate)Resources["ShipResource"]; }
        }

        private Style BorderBackgroundStyle { get { return (Style)Resources["PanelBackGroundStyle"]; } }


        public Constructor(NavyGrid navy)
        {
            InitializeComponent();
            MyTempShip.Type = ShipTypes.battleship; 
                
            MyTempShip.PropertyChanged += MyTempShip_PropertyChanged;

            MyNavy.AllShipsInstalled += MyNavy_AllShipsInstalled;
            FillUserPlaneBackgroundByBorders();
        
            // Задать модель
            MyNavy.SetNavy(navy);
            foreach(Ship shp in MyNavy.Ships)
            {
                if (shp != null)
                {
                    MyTempShip.CopyFrom(shp);
                    AddShipToView();
                }
            }
        }

        void MyNavy_AllShipsInstalled(object sender, EventArgs e)
        {
            MessageBox.Show("Все корабли установлены. Можно начинать бой.");
        }

        void MyTempShip_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (sender is ShipTemplate)
            {
                if (e.PropertyName == "Type")
                {
                    imgTempShipView.Source = BitmapFrame.Create(new Uri(string.Format("pack://application:,,,/BattleShips_WPF;component/Images/battleship-{0}.jpg", (int)MyTempShip.Type)));
                }
            }
        }

        private void RotateTempShipView(bool gorizontal)
        {
            if (gorizontal == true)
            {
                //((RotateTransform)imgTempShipView.RenderTransform).Angle = 0;              
                MyTempShip.Angle = 0;
            }
            else
            {
                //((RotateTransform)imgTempShipView.RenderTransform).Angle = 90;  
                MyTempShip.Angle = 90;
            }
            imgTempShipView.InvalidateVisual();
			// 
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (sender is CheckBox)
            {
                CheckBox cb = (CheckBox)sender;
                cb.Content = cb.IsChecked == true ? "Горизонтально" : "Вертикально";
                //RotateTempShipView(cb.IsChecked.Value);
            }
        }

        private void grdCreationPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            //TODO :Добавить отрисовку запрещающего кантика 
        }

        private void grdCreationPanel_MouseMove(object sender, MouseEventArgs e)
        {
            //TODO :Добавить отрисовку запрещающего кантика 
        }

        private void imgTempShipView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //var thumb = e.Source as UIElement;
            //var transform = thumb.RenderTransform as RotateTransform;
            //transform.Angle += 90;
        }

        private void FillMenuItem_Click(object sender, RoutedEventArgs e)
        {
        }

        private void FillUserPlaneBackgroundByBorders()
        {
            for (int j = 0; j < 10; j++)
            {
                for (int i = 0; i < 10; i++)
                {
                    var bord = new Border()
                    {
                        //BorderBrush = Brushes.GreenYellow,
                        Margin = new Thickness(0),
                        Name = string.Format("b{0}_{1}", i, j)
                    };
                    bord.SetValue(Grid.ColumnProperty, i + 1);
                    bord.SetValue(Grid.RowProperty, j + 1);
                    bord.MouseDown += bord_MouseDown;
                    bord.MouseEnter += bord_MouseEnter;
                    bord.MouseUp += bord_MouseUp;

                    bord.Style = BorderBackgroundStyle;

                    grdCreationPanel.Children.Add(bord);
                }
            }
        }

        void bord_MouseUp(object sender, MouseButtonEventArgs e)
        {
            lblXoY.Foreground = Brushes.Black;
        }

        void bord_MouseEnter(object sender, MouseEventArgs e)
        {
            if (sender is Border)
            {
                Border br = (Border)sender;

                int x, y;
                List<string> coord = br.Name.Split("b_".ToCharArray()).Where(s => s.Length > 0).ToList();


                if (int.TryParse(coord[0], out x))
                    if (int.TryParse(coord[1], out y))
                    {
                        lblXoY.Content = string.Format("({0}; {1})", x, y);
                    }
            }
        }

        void bord_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is Border)
            {
                Border br = (Border)sender;

                int x, y;
                List<string> coord = br.Name.Split("b_".ToCharArray()).Where(s => s.Length > 0).ToList();

                if (int.TryParse(coord[0], out x))
                    if (int.TryParse(coord[1], out y))
                    {
                        lblXoY.Content = string.Format("({0}; {1})", x, y);
                        lblXoY.Foreground = Brushes.Red;
                        MyTempShip.X = x;
                        MyTempShip.Y = y;


                        if (MyNavy.TryInsertShip(MyTempShip.Construct()))
                        {                            
                            AddShipToView();
                        }
                    }
            }
        }

        private void AddImgMenuItem_Click(object sender, RoutedEventArgs e)
        {
            AddShipToView();
        }

        private void AddShipToView()
        {
            //<Image Grid.ColumnSpan="4" Grid.Column="1"  Margin="1" Grid.RowSpan="1" 
            //Source="Images/battleship-1.jpg" Stretch="Fill" Grid.Row="1"/>
            
            var img = new Image();

            int colsp, rowsp;

            if (MyTempShip.Gorizontal == true)
            {
                colsp = MyTempShip.ShipLength;
                rowsp = 1;
            }
            else
            {
                colsp = 1;
                rowsp = MyTempShip.ShipLength;
            }

            img.SetValue(Grid.ColumnProperty, MyTempShip.X + 1);
            img.SetValue(Grid.ColumnSpanProperty, colsp);
            img.SetValue(Grid.RowProperty, MyTempShip.Y + 1);
            img.SetValue(Grid.RowSpanProperty, rowsp);

            var gc = img.GetValue(Grid.ColumnProperty);
            var gcs = img.GetValue(Grid.ColumnSpanProperty);
            var gr = img.GetValue(Grid.RowProperty);
            var grs = img.GetValue(Grid.RowSpanProperty);

            BitmapImage bitm = new BitmapImage(new Uri("Images/battleship-1.jpg", UriKind.Relative));

            if (MyTempShip.Angle == 90)
                bitm.Rotation = Rotation.Rotate90;
				
				// tempView.LayoutTransform = new RotateTransform(90);

            img.Source = bitm;
            img.Stretch = Stretch.Fill;

            grdCreationPanel.Children.Add(img);
        }

        private void imgBattleship_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MyTempShip.Type = ShipTypes.battleship;
        }

        private void imgCruiser_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MyTempShip.Type = ShipTypes.cruiser;
        }

        private void imgDestroyer_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MyTempShip.Type = ShipTypes.destroyer;
        }

        private void imgSubmarine_MouseDown(object sender, MouseButtonEventArgs e)
        {
            MyTempShip.Type = ShipTypes.submarine;
        }
    }
}
