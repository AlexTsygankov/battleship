﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.ComponentModel;

using BtSh_Model;

namespace BattleShips_WPF
{
    public class ShipsHolder : INotifyPropertyChanged
    {
        private Ship[] _ships;
        public Ship[] Ships
        {
            get
            {
                return _ships;
            }
            set
            {
                if (_ships != value)
                {
                    _ships = value;
                    NotifyPropertyChanged();
                }
            }
        }



        private Dictionary<ShipTypes, int> _shipsAvailable;
        public int BattleshipAvailable
        {
            get
            {
                return _shipsAvailable[ShipTypes.battleship];
            }
            set
            {
                if (_shipsAvailable[ShipTypes.battleship] != value)
                {
                    _shipsAvailable[ShipTypes.battleship] = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int CruiserAvailable
        {
            get
            {
                return _shipsAvailable[ShipTypes.cruiser];
            }
            set
            {
                if (_shipsAvailable[ShipTypes.cruiser] != value)
                {
                    _shipsAvailable[ShipTypes.cruiser] = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int DestroyerAvailable
        {
            get
            {
                return _shipsAvailable[ShipTypes.destroyer];
            }
            set
            {
                if (_shipsAvailable[ShipTypes.destroyer] != value)
                {
                    _shipsAvailable[ShipTypes.destroyer] = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int SubmarineAvailable
        {
            get
            {
                return _shipsAvailable[ShipTypes.submarine];
            }
            set
            {
                if (_shipsAvailable[ShipTypes.submarine] != value)
                {
                    _shipsAvailable[ShipTypes.submarine] = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        public ShipsHolder()
        {
            _shipsAvailable = new Dictionary<ShipTypes, int>() {
            { ShipTypes.battleship, 5-(int)ShipTypes.battleship}, 
            { ShipTypes.cruiser,    5-(int)ShipTypes.cruiser }, 
            { ShipTypes.destroyer,  5-(int)ShipTypes.destroyer },
            { ShipTypes.submarine,  5-(int)ShipTypes.submarine } };
            
            Ships = new Ship[10];
        }

        private NavyGrid _navy;
        public void SetNavy(NavyGrid NV)
        {
            _navy = NV;
            Ships = _navy.Ships;

            for (int i = 0; i < Ships.Length; i++)
                if (Ships[i] != null)
                {
                    TryDecreaseAvability(_navy.Ships[i]);
                }
        }
        public bool TryInsertShip(Ship shipCandidate)
        {
            ///////  1 Нос корабля
            int width = shipCandidate.Gorizontal == true ? (int)shipCandidate.Type : 1
                , height = shipCandidate.Gorizontal == false ? (int)shipCandidate.Type : 1;

            if (shipCandidate.X + width > 10 || shipCandidate.Y + height > 10)
                return false;
            if (shipCandidate.X < 0 || shipCandidate.Y < 0)
                return false;

            ///////  2 Весь корабль
            
            int right = (shipCandidate.X + width + 1) > 10 ? 10 : shipCandidate.X + width + 1;
            int bottom = (shipCandidate.Y + height + 1) > 10 ? 10 : shipCandidate.Y + height + 1;

            bool canInsert = true;

            for (int j = shipCandidate.Y == 0 ? 0 : shipCandidate.Y - 1;        j < bottom; j++)
            {
                for (int i = shipCandidate.X == 0 ? 0 : shipCandidate.X - 1;    i < right; i++)
                {
                    if (_navy.Cells[j, i] != CellStatuses.Empty)
                        canInsert = false;
                }
            }

            if (canInsert == false)
                return false;

            ///////  3 Вакантные места
            if (TryDecreaseAvability(shipCandidate) == false)
                return false;

            // Запомним ship
            for (int i = 0; i < Ships.Length; i++)
                if (Ships[i] == null)
                {
                    Ships[i] = shipCandidate;
                    break;
                }
            // Зарисуем клеточки
            ServiceInstrument.InstallShipIntoGrid(_navy.Cells, shipCandidate);

            return true;
        }

        private bool TryDecreaseAvability(Ship shipCandidate)
        {
            switch (shipCandidate.Type)
            {
                case ShipTypes.battleship:
                    if (BattleshipAvailable > 0)
                        BattleshipAvailable--;
                    else return false;
                    break;
                case ShipTypes.cruiser:
                    if (CruiserAvailable > 0)
                        CruiserAvailable--;
                    else return false;
                    break;
                case ShipTypes.destroyer:
                    if (DestroyerAvailable > 0)
                        DestroyerAvailable--;
                    else return false;
                    break;
                case ShipTypes.submarine:
                    if (SubmarineAvailable > 0)
                        SubmarineAvailable--;
                    else return false;
                    break;
                default:
                    return false;
            }           
            
            
            // Проверка на заполнение всех вакантных мест
            int count = 0;

            foreach (var availb in _shipsAvailable)
                count += availb.Value;

            if (count == 0)
                NotifyAllShipsInstalled();

            return true;
        }

        //private void Min<T>(ref T  arg1, ref T  arg2,  Func<T,T,bool>equalizer)
        //{
        //    if(!equalizer(arg1, arg2))
        //    {

        //    }

        //}
        //private T Max<T>(T a, T b,  Func<T,T,bool>equalizer)
        //{
        //    if(equalizer(a,b))
        //        return a;
        //    else return b;
        //}

        //private void Check_AllShipsIntstalled()
        //{
        //    int  count = 0;
        //    foreach (var elem in _shipsAvailable)
        //    {
        //        count += elem.Value;
        //    }

        //    if (count == 0)
        //        NotifyAllShipsInstalled();
        //}

        public delegate void AllShipsInstalledEventHandler(object sender, EventArgs e);
        public event AllShipsInstalledEventHandler AllShipsInstalled;
        private void NotifyAllShipsInstalled()
        {
            if (AllShipsInstalled != null)
            {
                AllShipsInstalled(this, new EventArgs());
            }
        }
    }
}
