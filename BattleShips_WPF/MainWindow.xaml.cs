﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BattleShips_WPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<Window> _OpenedWindows = new List<Window>();

        private BtSh_Model.NavyGrid PlayerShips, EnemyShips;

        public MainWindow()
        {
            InitializeComponent();
            PlayerShips = new BtSh_Model.NavyGrid();
            
        }

        private void menuBtn_withPC_Click(object sender, RoutedEventArgs e)
        {
            Constructor win = new Constructor(PlayerShips);
            _OpenedWindows.Add(win);           

            win.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (_OpenedWindows != null && _OpenedWindows.Count > 0)
            {
                foreach (Window w in _OpenedWindows)
                {
                    w.Close();
                }
            }

        }

        private void grdPlayerPanel_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void grdPlayerPanel_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void grdEnemyPanel_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void grdEnemyPanel_MouseMove(object sender, MouseEventArgs e)
        {

        }
    }
}
