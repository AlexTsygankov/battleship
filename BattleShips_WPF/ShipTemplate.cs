﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BtSh_Model;
using System.Windows.Media.Imaging;
using System.ComponentModel;

namespace BattleShips_WPF
{
    class ShipTemplate : INotifyPropertyChanged
    {


        private ShipTypes _shipType;
        public ShipTypes Type {
            get { return _shipType; }
            set {
                if (value != _shipType)
                {
                    _shipType = value;

                    NotifyPropertyChanged();
                }
            }
        }

        //private int _length;
        public int ShipLength
        {
            get
            {
                return (int)_shipType;
            }
            //set
            //{
            //    if (_length != value)
            //    {
            //        _length = value;
            //        NotifyPropertyChanged();
            //    }
            //}
        }

        private int _x, _y;
        public int X
        {
            get { return _x; }
            set
            {
                if (value != _x)
                {
                    _x = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public int Y
        {
            get { return _y; }
            set
            {
                if (value != _y)
                {
                    _y = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _gorizontal = true;
        public bool Gorizontal
        {
            get { return _gorizontal; }
            set
            {
                if (value != _gorizontal)
                {
                    _gorizontal = value;
                    NotifyPropertyChanged();

                    if (_gorizontal == true)
                        this.Angle = 0;
                    else Angle = 90;
                }
            }
        }

        private int _angle = 0;
        public int Angle
        {
            get { return _angle; }
            set
            {
                if (value != _angle)
                {
                    _angle = value;
                    NotifyPropertyChanged();
                }
            }
        }

        //private BitmapImage _icon;
        //public BitmapImage Icon
        //{
        //    get { return _icon; }
        //    set
        //    {
        //        if (value != _icon)
        //        {
        //            _icon = value;
        //            NotifyPropertyChanged();
        //        }
        //    }
        //}

        public void CopyFrom(Ship shp)
        {
            this.Gorizontal = shp.Gorizontal;
            this.X = shp.X;
            this.Y = shp.Y;
            this.Type = shp.Type;
        }
        


        public Ship Construct()
        {
            return new Ship(X, Y, Type, Gorizontal);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([System.Runtime.CompilerServices.CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
