﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Drawing;

namespace BattleShips_OOP
{
    public class Ship
    {
        public ShipTypes Type { get; set; }
        public bool Gorizontal { get; set; }
        public int Health { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        
        private Rectangle rect;                 // Прямоугольник размером с корабль

        public bool Contains(int x, int y) { return this.rect.Contains(x, y); }

        public Ship( ShipTypes type, bool gorizon)
        {
            this.Type = type;
            this.Gorizontal = gorizon;
            this.Health = (int)this.Type;
        }
        public Ship(int x, int y, ShipTypes type, bool gorizon)
        {
            this.X = x;
            this.Y = y;
            this.Type = type;
            this.Gorizontal = gorizon;
            this.Health = (int)type;

            if (this.Gorizontal)
                this.rect = new Rectangle(this.X, this.Y, (int)this.Type, 1);
            else 
                this.rect = new Rectangle(this.X, this.Y, 1, (int)this.Type);
        }
    }
}
