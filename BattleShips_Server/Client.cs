﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace BattleShips_Server
{
    class Client : IDisposable
    {
        List<Client> Parent;

        Thread thrProcess;
        TcpClient tcpClient;
        NetworkStream stream;
        DateTime ConnectTime;
        //string data;
        Byte[] bytes = new Byte[256];

        public bool Alive = false;

        int ID, _room;

        public string Ships { get { return _ships; } }
        private string _ships = "";

        public Client(TcpClient client, List<Client> parent, int room)
        {
            _room = room;
            this.Parent = parent;

            this.tcpClient = client;
            this.tcpClient.ReceiveTimeout = 300000;


            this.stream = client.GetStream();

            this.Alive = true;

            this.thrProcess = new Thread(new ThreadStart(Processing));
            this.ID = thrProcess.ManagedThreadId;
            thrProcess.Start();

            ConnectTime = DateTime.Now;
        }

        void Processing()
        {
            Console.WriteLine("Connected! ID = " + this.ID + '(' + this.ConnectTime + ')');
            Log.WriteLine("Connected! ID = " + this.ID + '(' + this.ConnectTime + ')');

            int i;
            try
            {
                // Loop to receive all the data sent by the client.
                while (this.Alive)
                {
                    if ((i = stream.Read(bytes, 0, bytes.Length)) == 0)
                        return;
                    else
                    {
                        // Translate data bytes to a UTF8 string.
                        string data = System.Text.Encoding.UTF8.GetString(bytes, 0, i);
                        Console.WriteLine("Received from <{1}>: {0}", data, this.ID);
                        Log.WriteLine("Received from <{1}>: {0}", data, this.ID);

                        ParseRecivedMessages(data);
                    }
                }
            }
            catch (IOException e)
            {
                // Такое исключение получается, когда сервер закрывается.
                // В одном потоке клиенты вырубаются, а в этом потоке всё ещё прослушиваются.
                SocketException t = (SocketException)e.InnerException;
                Console.WriteLine("Closing connection\nError(ID = " + this.ID + ") /n" + e.ToString());
                Log.WriteLine("Closing connection\nError(ID = " + this.ID + ") /n" + e.ToString());
            }
            finally
            {
                Console.WriteLine("Disconnected! ID = " + this.ID + "(" + this.ConnectTime + ')');
                Log.WriteLine("Disconnected! ID = " + this.ID + "(" + this.ConnectTime + ')');
                this.Parent.Remove(this);
                Dispose();
            }
        }

        private void ParseRecivedMessages(string message)
        {
            string[] parts = message.Split(" :".ToCharArray());

            if (parts[0] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ask])
            {

                if (parts[1] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game])
                {
                    if (parts[2] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships])
                    {
                        this._ships = parts[3];
                        bool enemy_ready = false;

                        Parent.ForEach(cl =>
                        {
                            if (cl.ID != this.ID && cl.Ships.Length > 0) enemy_ready = true;
                        });

                        if (enemy_ready)
                        {
                            SendToEnemy(string.Format("{0} {1} {2} {3}",
                                BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer],
                                BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game],
                                BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships],
                                this.Ships));
                            Parent.ForEach(cl =>
                            {
                                if (cl.ID != this.ID)
                                    this.Send(string.Format("{0} {1} {2} {3}",
                                        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer],
                                        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game],
                                        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships],
                                        cl.Ships));
                            });
                            Random rand = new Random();
                            bool turn = (rand.Next(1) == 1);

                            SendToEnemy(string.Format("{0} {1} {2}",
                                BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer],
                                BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.shoot],
                                turn));
                            Send(string.Format("{0} {1} {2}",
                                BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer],
                                BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.shoot],
                                !turn));
                        }
                    }
                }
                else if (parts[1] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.shoot])
                {
                    SendToEnemy(message);
                }
            }
            else if (parts[0] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer])
            {
                //if (parts[1] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game])
                //{
                //    //if (parts[2] == BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships])
                //        //NewNetGame(parts[3]);
                //}
            }
        }
        public void SendToEnemy(string message)
        {
            Parent.ForEach(x =>
            {
                if (x.ID != this.ID)
                    x.Send(message);
            });
        }
        public void Send(string Message)
        {
            byte[] msg = System.Text.Encoding.UTF8.GetBytes(Message);

            // Send back a response.
            try
            {
                stream.Write(msg, 0, msg.Length);
                Console.WriteLine("SentTo<{1}>:' {0}", Message, this.ID);
                Log.WriteLine("SentTo<{1}>:' {0}", Message, this.ID);
            }
            catch (Exception e)
            {
                Console.WriteLine("Trying SentTo<{1}>:' {0} \n Get Error", Message, this.ID);
                Console.WriteLine(e);
                Log.WriteLine("Trying SentTo<{1}>:' {0} \n Get Error", Message, this.ID);
                Log.WriteLine("{0}", e);
            }
        }

        public override string ToString()
        {
            return
            "RemovePoint(" + tcpClient.Client.RemoteEndPoint.ToString() +
            ") LocalPoint(" + tcpClient.Client.LocalEndPoint.ToString() + ")";
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (stream.CanWrite)
                {
                    Send("Close connection");

                    this.Alive = false;
                    //thrProcess.Abort();
                    Parent.Remove(this);
                    stream.Dispose();
                    tcpClient.Close();


                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
