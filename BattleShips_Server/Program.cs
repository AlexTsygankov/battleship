﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace BattleShips_Server
{
    static class Log
    {
        private static StreamWriter sw ;
        public static void Write(string s, params object[] args)
        {
            sw.Write(s, args);
        }
        public static void WriteLine(string s, params object[] args)
        {
            sw.WriteLine(s, args);
        }
        public static void Create() {
            string s = "Log_" + DateTime.Now.ToString().Replace(' ', '-').Replace(':', '.') + ".txt";
            sw = new StreamWriter(s);

            Console.WriteLine("Log created '{0}'", s);
            
        }
        public static void Close() {
            sw.Close();
        }
    }


    class Program
    {

        //private enum MenuCommands : byte { Help, Show_all_clients, shw_clnts_in_rooms }
        //private const Dictionary<MenuCommands, string> commands = new Dictionary<MenuCommands, string>()
        //{
        //    {MenuCommands.Help, "HELP"},
        //    {MenuCommands.Show_all_clients, "SHW_ALL_CLNTS"},
        //    {MenuCommands.shw_clnts_in_rooms, "SHW_ROOM_CLNTS"}
        //};

        static TcpListener listener = null;
        static Thread thrClientsAdder = null;
        static List<Client> clients = null;
        static List<List<Client>> rooms = null;

        static Int32 Port;
        static string Host;

        static void Main(string[] args)
        {
            Log.Create();
            clients = new List<Client>();
            rooms = new List<List<Client>>();

            thrClientsAdder = new Thread(new ThreadStart(AddingNewClients));
            thrClientsAdder.Name = "___New clients adder";
            thrClientsAdder.Start();

            //Console.WriteLine("Введите порт для старта");
            Port = 13000;

            

            string command;
            bool Play = true;

            Console.WriteLine("For help type '?' ");
            while (Play)
            {
                Console.Write("> ");
                command = Console.ReadLine();
                switch (command.ToUpper())
                {
                    case "/?":
                    case "?":
                    case "HELP":
                        Console.WriteLine("\nДоступные команды:");
                        Console.WriteLine("HELP | /? | ?\t- Это меню");
                        Console.WriteLine("SHOW_ALL_CLIENTS");
                        Console.WriteLine("REWRTLOG");
                        Console.WriteLine("SHW_CLNTS");
                        Console.WriteLine("WRLOG");
                        Console.WriteLine("EXIT\t- Закрывает сервер");
                        break;

                    case "SHOW_ALL_CLIENTS":
                        //if (clients.Count > 0)
                        //    clients.ForEach(a => Console.WriteLine(a.ToString()));
                        //else
                        //    Console.WriteLine("Нет подлюченных клиентов.");
                        for (int i = 0; i < rooms.Count; i++)
                        {
                            Console.WriteLine("room[{0}]", i);
                            if (rooms[i].Count > 0)
                            {
                                rooms[i].ForEach(client => Console.WriteLine("\tclient {0}", client.ToString()));
                            }
                        }
                        break;
                    case "SHW_CLNTS":
                        //if (clients.Count > 0)
                        //    clients.ForEach(a => Console.WriteLine(a.ToString()));
                        //else
                        //    Console.WriteLine("Нет подлюченных клиентов.");
                        for (int i = 0; i < rooms.Count; i++)
                        {
                            Console.WriteLine("room[{0}]", i);
                            Log.WriteLine("room[{0}]", i);
                            if (rooms[i].Count > 0)
                            {
                                rooms[i].ForEach(client => { 
                                    Console.WriteLine("\tclient {0}", client.ToString());
                                    Log.WriteLine("\tclient {0}", client.ToString());
                                });
                            }
                        }
                        break;
                    case "WRLOG":
                        Log.Write("test|test {0}  {1}", 5*12.3, '&'+"los");

                        Log.Write("\nnewtest|newtest");

                        break;
                    case "REWRTLOG":
                        Log.Close();
                        Log.Create();
                        Console.WriteLine("Log restarted!");
                        break;

                    case "EXIT":
                        Play = false;
                        //clients.ForEach(a => a.Dispose());
                        //listener.Stop();
                        thrClientsAdder.Abort();
                        break;


                    default:
                        Console.WriteLine("Command doesn't exist. For help write 'help' or '?'");
                        break;
                }
            }

            while (thrClientsAdder.IsAlive)
                Thread.Sleep(500);

            Log.Close();
            Console.WriteLine("\nHit enter to continue...");
            Console.Read();

        }

        private static void AddingNewClients()
        {
            try
            {
                Console.WriteLine("Данные для старта");
                //Console.Write("Хост: "); Host = Console.ReadLine();
                //Console.Write("Порт: "); Port = Int32.Parse(Console.ReadLine());
                Console.WriteLine(IPAddress.Any);
                listener = new TcpListener(IPAddress.Any, 13000);
                listener.Start();

                Console.WriteLine("Waiting for a connection... ");
                Log.WriteLine("Waiting for a connection... ");
                while (true)
                {
                    //Perform a blocking call to accept requests.
                    //You could also user server.AcceptSocket() here.

                    //TcpClient client = listener.AcceptTcpClient();
                    if (listener.Pending())
                    {
                        //clients.Add(new Client(listener.AcceptTcpClient(), clients));
                        int index = -1;
                        for(int i = 0; i < rooms.Count; i++)
                            if(rooms[i].Count < 2){ index = i; break;}
                        if (index == -1)
                        {
                            rooms.Add(new List<Client>());
                            //room = rooms[rooms.Count - 1];
                            index = rooms.Count-1;
                        }
                        rooms[index].Add(new Client(listener.AcceptTcpClient(), rooms[index], index));
                        
                        //bool all_ships_ready_to_start = true;
                        //rooms[index].ForEach(cl => all_ships_ready_to_start = all_ships_ready_to_start && cl.Ships.Length > 0);

                        //if (all_ships_ready_to_start && rooms[index].Count >= 2)
                        //    rooms[index].ForEach(cl => cl.SendToEnemy(string.Format("{0} {1} {2} {3}",
                        //        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.answer],
                        //        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.start_game],
                        //        BtShpProtocol.ProtocolDictionary[BtShpProtocol.MessagesProtocol.ships],
                        //        cl.Ships)));
                    }
                    Thread.Sleep(100);
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e.ToString());
                Log.WriteLine("SocketException: {0}", e.ToString());
            }
            finally
            {
                // Stop listening for new clients.
                listener.Stop();
                clients.ForEach(a => a.Dispose());
                rooms.ForEach(room => room.ForEach(client => client.Dispose()));
            }
        }

        
    }


    
}


